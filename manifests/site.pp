# create a new run stage to ensure certain modules are included first
stage { 'pre':
  before => Stage['main']
}

# add the baseconfig module to the new 'pre' run stage
class { 'baseconfig':
  stage => 'pre'
}

# set defaults for file ownership/permissions
File {
  owner => 'root',
  group => 'root',
  mode  => '0644',
}

# all boxes get the base config
include baseconfig

# box.
node 'box.local.logicitlab.com' {
  class { 'nginx': }
  class { 'yum':
    extrarepo => [ 'epel', 'remi', 'mariadb', 'puppetlabs' ],
  }

  class { 'activemq':
    webconsole => true,
  }

  ## MariaDB 
  ## MySQL server
  file { "/var/run/mysqld":
    ensure => "directory",
    owner  => "mysql",
    group  => "mysql",
    mode   => 750,
  }

  class { 'mysql::server':
    package_name      => 'MariaDB-server',
    service_enabled   => true,
    service_manage    => true,
    service_name      => 'mysql',
    require           => Class[yum::repo::mariadb]
  }

  class { 'mysql::client':
    package_name => 'MariaDB-client',

    require => Class[yum::repo::mariadb]
  }

  class { 'mysql::bindings':
    php_package_name  => 'php-mysqlnd',
  }

  ## PHP 5.4
  class { 'php': 
    service => 'nginx',
    require => Class[yum::repo::remi]
  }

  php::module { "common": }
  php::module { "fpm": }
  php::module { "gd": }
  php::module { "imap": }
  php::module { "intl": }
  php::module { "mcrypt": }
  php::module { "xcache": }
  php::module { "xml": }
  php::module { "xmlrpc": }


  ## MongoDB
  class {'::mongodb::globals':
    manage_package_repo => true,
  }->
  class {'::mongodb::server': }->
  class {'::mongodb::client': }
}

#node 'box.local.logicitlab.com' { # [1]
#            class { 'mysql::server':  # [2]
#                    config_hash => { 'root_password' => 'herpderpderp' },
#            }
#            include mysql::php # [3]

            # Configuring apache
#            include apache # [4]
#            include apache::mod::php

#            apache::vhost { $::fqdn: # [5]
#                    port => '80',
#                    docroot => '/var/www/test',
#                    require => File['/var/www/test'],
#            }

#            # Setting up the document root
#            file { ['/var/www', '/var/www/test'] : # [6]
#                    ensure => directory,
#            }

#            file { '/var/www/test/index.php' : # [7]
#                    content => '>?php echo \'>p<Hello world!>/p<\' ?<',
#            }

            # "Realize" the firewall rule
#            Firewall <| |> # [8]
#}
